#!/bin/sh

set -e
set -u

port="${1:-8080}"
cgi_server=cgiserver

if ! test -d "${cgi_server}"; then
	git clone https://github.com/klange/cgiserver.git "${cgi_server}"
	make -C "${cgi_server}"
fi
cp -a www/* "${cgi_server}/pages"
cd "${cgi_server}"
./cgiserver ${port}
